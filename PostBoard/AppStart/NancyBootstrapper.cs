﻿using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using PostBoard.Repository;

namespace PostBoard.AppStart
{
    public class NancyBootstrapper : DefaultNancyBootstrapper
    {
        
        protected new void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
        }
        
        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            container.Register<ClientRepo>().AsSingleton();
            base.ConfigureApplicationContainer(container);
        }
    }
}