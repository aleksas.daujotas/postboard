﻿using System;

namespace PostBoard.Model.Client
{

    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Comment { get; set; }
    }
    
    public class Action
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string ActionType { get; set; }
        public DateTime Timestamp { get; set; }
    }
}