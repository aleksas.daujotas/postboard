﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using PostBoard.Repository.Helper;
using Action = PostBoard.Model.Client.Action;
using Client = PostBoard.Model.Client.Client;

namespace PostBoard.Repository
{
    public class ClientRepo
    {
        public IEnumerable<Client> GetAllClients()
        {
            var clients = new List<Client>();
            using (var conn = DatabaseHelper.GetConnection())
            {
                conn.Open();
                using (var cmd = new SQLiteCommand("SELECT * FROM Clients", conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            clients.Add(new Client
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Age = reader.GetInt32(2),
                                Comment = reader.IsDBNull(3) ? null : reader.GetString(3)
                            });
                        }
                    }
                }
            }

            return clients;
        }

        public Client GetClientById(int id)
        {
            Client client = null;
            using (var conn = DatabaseHelper.GetConnection())
            {
                conn.Open();
                using (var cmd = new SQLiteCommand("SELECT * FROM Clients WHERE Id = @Id", conn))
                {
                    cmd.Parameters.AddWithValue("@Id", id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            client = new Client
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Age = reader.GetInt32(2),
                                Comment = reader.IsDBNull(3) ? null : reader.GetString(3)
                            };
                        }
                    }
                }
            }

            Debug.Assert(client != null, nameof(client) + " == null");
            return client;
        }

        public void AddClient(Client client)
        {
            using (var conn = DatabaseHelper.GetConnection())
            {
                conn.Open();
                using (var cmd = new SQLiteCommand(
                           "INSERT INTO Clients (Name, Age, Comment) VALUES (@Name, @Age, @Comment)", conn))
                {
                    cmd.Parameters.AddWithValue("@Name", client.Name);
                    cmd.Parameters.AddWithValue("@Age", client.Age);
                    cmd.Parameters.AddWithValue("@Comment", client.Comment);
                    cmd.ExecuteNonQuery();
                    client.Id = (int)conn.LastInsertRowId;
                }

                LogAction(client.Id, "Registered");
            }
        }

        public void UpdateClient(Client client)
        {
            using (var conn = DatabaseHelper.GetConnection())
            {
                conn.Open();
                using (var cmd = new SQLiteCommand(
                           "UPDATE Clients SET Name = @Name, Age = @Age, Comment = @Comment WHERE Id = @Id", conn))
                {
                    cmd.Parameters.AddWithValue("@Name", client.Name);
                    cmd.Parameters.AddWithValue("@Age", client.Age);
                    cmd.Parameters.AddWithValue("@Comment", client.Comment);
                    cmd.Parameters.AddWithValue("@Id", client.Id);
                    cmd.ExecuteNonQuery();
                }

                LogAction(client.Id, "Edited");
            }
        }

        public void DeleteClient(int id)
        {
            using (var conn = DatabaseHelper.GetConnection())
            {
                conn.Open();
                using (var cmd = new SQLiteCommand("DELETE FROM Clients WHERE Id = @Id", conn))
                {
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.ExecuteNonQuery();
                }

                LogAction(id, "Deleted");
            }
        }

        public IEnumerable<Action> GetActionsForClient(int clientId)
        {
            var actions = new List<Action>();
            using (var conn = DatabaseHelper.GetConnection())
            {
                conn.Open();
                using (var cmd = new SQLiteCommand("SELECT * FROM Actions WHERE ClientId = @ClientId", conn))
                {
                    cmd.Parameters.AddWithValue("@ClientId", clientId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            actions.Add(new Action
                            {
                                Id = reader.GetInt32(0),
                                ClientId = reader.GetInt32(1),
                                ActionType = reader.GetString(2),
                                Timestamp = reader.GetDateTime(3)
                            });
                        }
                    }
                }
            }

            return actions;
        }

        private void LogAction(int clientId, string actionType)
        {
            using (var conn = DatabaseHelper.GetConnection())
            {
                conn.Open();
                using (var cmd = new SQLiteCommand(
                           "INSERT INTO Actions (ClientId, ActionType) VALUES (@ClientId, @ActionType)", conn))
                {
                    cmd.Parameters.AddWithValue("@ClientId", clientId);
                    cmd.Parameters.AddWithValue("@ActionType", actionType);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}