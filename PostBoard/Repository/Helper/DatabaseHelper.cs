﻿using System.Data.SQLite;
using System.IO;

namespace PostBoard.Repository.Helper
{
    public class DatabaseHelper
    {
        private const string DbFile = "clients.db";

        public static SQLiteConnection GetConnection()
        {
            if (!File.Exists(DbFile))
            {
                SQLiteConnection.CreateFile(DbFile);
            }

            return new SQLiteConnection($"Data Source={DbFile};Version=3;");
        }

        public static void InitializeDatabase()
        {
            var connection = GetConnection();
            connection.Open();
            using (var cmd = new SQLiteCommand("PRAGMA foreign_keys = ON;", connection))
            {
                cmd.ExecuteNonQuery();
            }

            const string createClientTable = @"
                                            CREATE TABLE IF NOT EXISTS Clients (
                                            Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                            Name TEXT NOT NULL,
                                            Age INTEGER NOT NULL,
                                            Comment TEXT
                                            )
                                         ";

            const string createActionsTable = @"
                                            CREATE TABLE IF NOT EXISTS Actions (
                                            Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                            ClientId INTEGER NOT NULL,
                                            ActionType TEXT NOT NULL,
                                            Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
                                            FOREIGN KEY (ClientId) REFERENCES Clients(Id)
                                            )
                                          ";

            using (var cmd = new SQLiteCommand(createClientTable, connection))
            {
                cmd.ExecuteNonQuery();
            }

            using (var cmd = new SQLiteCommand(createActionsTable, connection))
            {
                cmd.ExecuteNonQuery();
            }
        }
    }
}