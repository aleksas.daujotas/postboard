﻿using System.Linq;
using Nancy;
using Nancy.ModelBinding;
using PostBoard.Model.Client;
using PostBoard.Repository;

namespace PostBoard.Modules
{

    public class ClientModule : NancyModule
    {
        private ClientRepo _repository;

        public ClientModule(ClientRepo repository) : base("")
        {
            _repository = repository;
            Get("/1234", _ => "Hello, Nancy!");
            Get("/clients", _ => Response.AsJson(_repository.GetAllClients()));

            Get("/clients/{id:int}", parameters =>
            {
                int id = parameters.id;
                var client = _repository.GetClientById(id);
                return Response.AsJson(client);
            });

            Post("/clients", _ =>
            {
                var client = this.Bind<Client>();
                _repository.AddClient(client);
                return Response.AsJson(client);
            });

            Put("/clients/{id:int}", parameters =>
            {
                int id = parameters.id;
                var client = this.Bind<Client>();
                client.Id = id;
                _repository.UpdateClient(client);
                return Response.AsJson(client);
            });

            Delete("/clients/{id:int}", parameters =>
            {
                int id = parameters.id;
                _repository.DeleteClient(id);
                return HttpStatusCode.NoContent;
            });

            Get("/clients/{id:int}/actions", parameters =>
            {
                int id = parameters.id;
                var actions = _repository.GetActionsForClient(id).ToArray();
                return actions.Length == 0 ? HttpStatusCode.NotFound : Response.AsJson(actions);
            });
        }
    }
}