using System;
using Nancy.Hosting.Self;
using PostBoard.AppStart;
using PostBoard.Repository.Helper;

namespace PostBoard
{
    namespace PostBoard
    {
        public class Program
        {
            static void Main(string[] args)
            {
                var uri = new Uri("http://localhost:1234");
                var hostConfig = new HostConfiguration();
                hostConfig.UrlReservations.CreateAutomatically = true;

                using (var nancyHost = new NancyHost(uri, new NancyBootstrapper(), hostConfig))
                {
                    nancyHost.Start();
                    DatabaseHelper.InitializeDatabase();
                    Console.WriteLine($"NancyFX is running on {uri}");
                    Console.ReadLine();
                }
            }
        }
    }
}